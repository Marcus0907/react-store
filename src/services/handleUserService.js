const API = 'http://localhost:3000/users'

class HandleUserService{

    constructor(){
       // this.getListUser()
    }

    getListUser() {
        return fetch(API);
    }

    createUser(user){
        let obj = {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(user)
        }

        return fetch(API, obj)
    }

    updateUser(id, user){

        let obj = {
            method: 'put',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        }

        return fetch(`${API}/${id}`, obj)
    }

    deleteUser(id){

        let obj = {
            method: 'delete',
        }

        return fetch(`${API}/${id}`, obj)

    }

}

export default HandleUserService;