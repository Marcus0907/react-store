import React from "react";
import "./listUsers.css";
import User from "../../models/User";
import HandleUserService from '../../services/handleUserService';
import imgMarcus from "../../imagensBD/Marcus.jpeg"
import imgMateus from "../../imagensBD/Mateus.jpg"
import imgMarcia from "../../imagensBD/Marcia.jpg"
import imgMaria from "../../imagensBD/Maria.jpg"
import imgMaalton from "../../imagensBD/Maalton.jpg"
import placeholder from "../../imagensBD/placeholder.jpg"

class ListUsers extends React.Component {
  constructor(props) {
    super(props);

    this.state ={
      userList:[]
    }
  }

  componentDidMount(){
    const service = new HandleUserService();
    service.getListUser().then(res => res.json()).then(data => this.setState({userList:data}))
    
  }

  setImage(position){
    if(position === 1)
      return imgMarcus;
    else if(position === 2)
      return imgMaria;
    else if(position === 3)
      return imgMarcia;
    else if(position === 4)
      return imgMateus;
    else if(position === 5)
      return imgMaalton;
    else  return placeholder;

    
      
  }

   render() {
    
    return (
      <div className="container mt-5" style={{backgroundColor: '#b0b0ff', borderRadius:'3px'}}>
        <div className="row ">
            <div className="col">
                <h2>List of Users</h2>
            </div>
        </div>  
        <div className="row mb-4">

          {this.state.userList.map(user => 

            <div className="col" key={user.id}>
              <div className="card mb-4"  >
                <img src={this.setImage(user.id)} style={{ height:200}} className="card-img-top" alt="..."/>
                  <div className="card-body">
                    <p className="card-text">
                     {user.name}
                    </p>
                    <p className="card-text">
                     {user.age}
                    </p>
                   
                  </div>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default ListUsers;
