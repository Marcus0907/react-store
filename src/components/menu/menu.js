import React from 'react';
import './menu.css';
import {  Link } from 'react-router-dom'

class Menu extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <nav className="navbar navbar-expand-sm navbar-light bg-light menu-container" >
            <a className="navbar-brand" href="#">React App</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item ">
                  <Link className="nav-link" to="/">List <span className="sr-only">(current)</span></Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/create">Create User</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/update">Update User</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/delete">Delete User</Link>
                </li>
              </ul>
            </div>
          </nav>
        );
    }
}

export default Menu;