import React from 'react';
import HandleUserService from '../../services/handleUserService';

class UpdateUser extends React.Component{
    constructor(props){
        super(props)

        this.state = {
            name:'',
            age:0,
            id:undefined,
            userList:[],
            disableInputs: true
        }
    }

    componentDidMount(){
        const service = new HandleUserService();
        service.getListUser().then(res => res.json()).then(data => this.setState({userList:data}))
        
      }

    handleSubmit = (e) => {
        e.preventDefault();

        let name = this.state.name;
        let age = this.state.age;
        let id = this.state.id;

        let user = {
            name,
            age
        }

        if(name && age){
            const service = new HandleUserService();
            service.updateUser(id,user).then(res => res.json()).then(data => console.log(data))
        }

    }

    handleChange = (e) => {

        let type = e.target.name;
        let value = e.target.value;

        if(type === 'name')
            this.setState({name:value});
        else
            this.setState({age:value});    

    }

    handleSelectChange = (e) => {
        let value = e.target.value
        let user;
        
        console.log(value);
        if(value !== "0"){

            user = this.state.userList.filter(user => user.id == value)

            console.log(user);
            
            this.setState({id:value, disableInputs:false, name:user[0].name, age:user[0].age}, () => console.log(this.state));   

           
            
        }
            
    }

    render(){
        return(
            <div className="container p-5 mt-5" style={{backgroundColor: '#b0b0ff', borderRadius:'3px'}}>
                <div className="row ">
                    <div className="col">
                        <h2>Update User</h2>
                    </div>
                </div>  
                <div className="row justify-content-center">
                   
                <form onSubmit={(e) => this.handleSubmit(e)}>
                    <div className="form-group">
                        <label >id</label>
                        <select className="form-control" value={this.state.id} onChange={(e) => this.handleSelectChange(e)}>
                            <option value={0}>select an id</option>
                            {
                                this.state.userList.map(user => 
                                
                                    <option key={user.id} value={user.id}>{user.id}</option>

                                )
                            }
                        </select>
                    </div>
                    <div className="form-group">
                        <label >Nome</label>
                        <input value={this.state.name} disabled={this.state.disableInputs} onChange={(e) => this.handleChange(e)} type="text" className="form-control" id="inputNome"  placeholder="Enter name" name="name" />
                    </div>
                    <div className="form-group">
                        <label >Age</label>
                        <input  value={this.state.age} disabled={this.state.disableInputs} onChange={(e) => this.handleChange(e)} type="number" className="form-control" id="inputAge" placeholder="Age" name="age" />
                    </div>

                    
                    
                    <button type="submit" className="btn btn-primary">Update</button>
                </form>
                
                </div>
            </div>
        )
    }
}

export default UpdateUser;