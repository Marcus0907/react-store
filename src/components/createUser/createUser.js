import React from 'react';
import './createUser.css';

import HandleUserService from '../../services/handleUserService';

class CreateUser extends React.Component{
    constructor(props){
        super(props);

        this.state ={
            name:'',
            age:0
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();

        let name = this.state.name;
        let age = this.state.age;

        let user = {
            name,
            age
        }

        console.log(user);

        if(name && age){
            const service = new HandleUserService();
            service.createUser(user).then(res => res.json()).then(data => console.log(data))

            this.setState({name:'', age:''})
        }

    }

    handleChange = (e) => {

        let type = e.target.name;
        let value = e.target.value;

        if(type === 'name')
            this.setState({name:value});
        else
            this.setState({age:value});    

    }

    render(){
        return(
            <div className="container p-5 mt-5" style={{backgroundColor: '#b0b0ff', borderRadius:'3px'}}>
                <div className="row ">
                    <div className="col">
                        <h2>Create User</h2>
                    </div>
                </div>  
                <div className="row justify-content-center">
                   
                <form onSubmit={(e) => this.handleSubmit(e)}>
                    <div className="form-group">
                        <label >Nome</label>
                        <input value={this.state.name} onChange={(e) => this.handleChange(e)} type="text" className="form-control" id="inputNome" aria-describedby="nome" placeholder="Enter name" name="name" />
                    </div>
                    <div className="form-group">
                        <label >Age</label>
                        <input value={this.state.age} onChange={(e) => this.handleChange(e)} type="number" className="form-control" id="inputAge" placeholder="Age" name="age" />
                    </div>

                    
                    
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
                
                </div>
                <div className="alert alert-warning alert-dismissible fade hide" role="alert">
                    <strong>Holy guacamole!</strong> You should check in on some of those fields below.
                    <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        )
    }
}

export default CreateUser;