import React from 'react';
import HandleUserService from '../../services/handleUserService';

class DeleteUser extends React.Component{
    constructor(props){
        super(props)

        this.state = {
            name:'',
            id:undefined,
            userList:[],
            disableBtn:true
          
        }
    }

    componentDidMount(){
        const service = new HandleUserService();
        service.getListUser().then(res => res.json()).then(data => this.setState({userList:data}))
      }

    handleSubmit = (e) => {
        e.preventDefault();

        if(!this.state.disableBtn){
            const service = new HandleUserService();
            service.deleteUser(this.state.id).then(res => res.json()).then(data => console.log(data))
            let users = this.state.userList.filter(user => user.id != this.state.id)
            this.setState({name:'', userList:users})
        }

    }

   

    handleSelectChange = (e) => {
        let value = e.target.value
        let user, users;
        
        console.log(value);
        if(value !== "0"){

            user = this.state.userList.filter(user => user.id == value)
            
            this.setState({id:value, disableBtn:false, name:user[0].name}, () => console.log(this.state));
            
        }
            
    }

    render(){
        return(
            <div className="container p-5 mt-5" style={{backgroundColor: '#b0b0ff', borderRadius:'3px'}}>
                <div className="row ">
                    <div className="col">
                        <h2>Delete User</h2>
                    </div>
                </div>  
                <div className="row justify-content-center">
                   
                <form onSubmit={(e) => this.handleSubmit(e)}>
                    <div className="form-group">
                        <label >id</label>
                        <select className="form-control" value={this.state.id} onChange={(e) => this.handleSelectChange(e)}>
                            <option value={0}>select an id</option>
                            {
                                this.state.userList.map(user => 
                                
                                    <option key={user.id} value={user.id}>{user.id}</option>

                                )
                            }
                        </select>
                    </div>
                    <div className="form-group">
                        <label >Nome</label>
                        <input value={this.state.name} disabled={true}  type="text" className="form-control" id="inputNome"  placeholder="name" name="name" />
                    </div>
                    
                    <button disabled={this.state.disableBtn} type="submit" className="btn btn-primary">Delete</button>
                </form>
                
                </div>
            </div>
        )
    }
}

export default DeleteUser;