import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom'

import ListUsers from './components/listarUsers/listUsers'
import CreateUser from './components/createUser/createUser';
import Menu from './components/menu/menu';
import UpdateUser from './components/updateUser/updateUser'
import DeleteUser from './components/deleteUser/deleteUser'

function App() {
  return ( 
  <div> 
    <div><Menu/></div>
    <div><ListUsers/></div>
    
  </div>
  )
}


ReactDOM.render(
  <BrowserRouter>
    <Menu/>
    <Switch>
      <Route path="/" exact={true} component={ListUsers}/>
      <Route path="/create" component={CreateUser}/>
      <Route path="/update" component={UpdateUser}/>
      <Route path="/delete" component={DeleteUser}/>
    </Switch>
  </BrowserRouter>
,
  document.getElementById('root')
);

